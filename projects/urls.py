from django.contrib import admin
from django.urls import path
from projects.views import list_projects

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", list_projects, name="home"),
]
