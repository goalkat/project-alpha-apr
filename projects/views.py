from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required


@login_required
def list_projects(request):
    list_projects = Project.objects.filter(owner=request.user)

    context = {
        "list_projects": list_projects,
    }

    return render(request, "projects/list.html", context)


def redirect_to_home(request):
    return redirect("home")
