from django.contrib import admin
from django.urls import path
from accounts.views import user_login, user_logout

from accounts.views import user_signup

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", user_signup, name="signup"),
]
